import numpy as np
from Layer import Layer


class NeuralNetwork:
    def __init__(
        self,
        features,
        labels,
        learning_rate=0.0001,
        epochs=1000,
        should_shuffle=True,
        rand_seed=0
    ):
        self.features = features
        self.labels = labels
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.layers = []
        self.should_shuffle = should_shuffle
        np.random.seed(rand_seed)

    def add_layer(self, layer):
        layers_count = len(self.layers)

        if layers_count > 0:
            self.layers[layers_count-1].next_layer = layer

        self.layers.append(layer)

    def compile(self):
        for layer in self.layers:
            layer.configure()

    def train(self):
        for i in range(self.epochs):
            if self.should_shuffle:
                self.shuffle()

            for j in range(self.features.shape[0]):
                self.forward_propagate(self.features[j])
                self.backward_propagate(self.labels[j])
                self.optimize_weights()

    def predict(self, feature):
        self.forward_propagate(feature)

        return self.layers[-1].output

    def forward_propagate(self, feature):
        input = feature

        for layer in self.layers:
            layer.input = input
            layer.compute_output()
            input = layer.output

    def backward_propagate(self, output):
        prev_layer = self.layers[-1]
        prev_layer.deltas.fill(2*(prev_layer.output[0] - output[0]))

        for layer in self.layers[:-1][::-1]:
            layer.compute_deltas(prev_layer.weights, prev_layer.deltas)
            prev_layer = layer

    def optimize_weights(self):
        for layer in self.layers[::-1]:
            layer.optimize_weights(self.learning_rate)

    def shuffle(self):
        permutation = np.random.permutation(len(self.features))

        self.features = self.features[permutation]
        self.labels = self.labels[permutation]


f = np.array(range(20), dtype='float64').reshape(20, 1)
o = np.ones(shape=(20, 1))
l = np.array([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71], dtype='float64').reshape(20, 1)

fo = np.hstack((f, o))

model = NeuralNetwork(fo, l)

model.add_layer(Layer(4, input_dim=2, activation='tanh'))
model.add_layer(Layer(input_dim=4, activation='linear'))
model.compile()
model.train()

model.predict([1, 1])
