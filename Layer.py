import numpy as np
from activations.Activation import Activation


class Layer:
    def __init__(
        self,
        units=1,
        input_dim=1,
        activation='linear',
    ):
        self.units = units
        self.input_dim = input_dim
        self.weights = None
        self.input = None
        self.weighted_output = None
        self.output = None
        self.deltas = None
        self.next_layer = None
        self.activation = Activation.get(activation)

    def configure(self):
        self.weights = np.random.rand(self.units, self.input_dim)
        self.deltas = np.ones(self.units)

    def compute_output(self):
        self.weighted_output = np.array([np.dot(self.input, node_weights) for node_weights in self.weights])

        self.output = np.array([self.activate(weighted_output) for weighted_output in self.weighted_output])

    def compute_deltas(self, weights, deltas):
        self.deltas = np.array([self.derive(x) for x in self.weighted_output]) * (weights.T @ deltas)

    def optimize_weights(self, learning_rate):
        self.weights -= np.outer(self.input, self.deltas).T * learning_rate

    def activate(self, x):
        return self.activation.activate(x)

    def derive(self, x):
        return self.activation.derive(x)
