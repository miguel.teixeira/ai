from activations.Relu import Relu
from activations.Tanh import Tanh
from activations.Linear import Linear
from activations.Sigmoid import Sigmoid


class Activation:
    @staticmethod
    def get(activation):
        if activation == 'relu':
            return Relu()

        if activation == 'Sigmoid':
            return Sigmoid()

        if activation == 'tanh':
            return Tanh()

        return Linear()
