class Relu:
    @staticmethod
    def activate(x):
        return max(0, x)

    @staticmethod
    def derive(x):
        return 0 if x < 0 else 1

