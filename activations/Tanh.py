import numpy as np


class Tanh:
    @staticmethod
    def activate(x):
        return np.tanh(x)

    @staticmethod
    def derive(x):
        return 1 - Tanh.activate(x) ** 2
